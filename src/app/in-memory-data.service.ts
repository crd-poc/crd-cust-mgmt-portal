import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';
import { Customer } from './customer';
@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const customers = [
      {
			  "id": 11,
			  "name": "Muster GmbH",
			  "taxNumber": 4711,
			  "addressLine1": "Prag Street 12",
			  "addressLine2": "10999",
			  "location": "Berlin",
			  "legalForm": "GmBH",
			  "public": false,
			  "industryCodeISIC": "49410",
			  "industryCodeLocal": "49410",
			  "customerType": "Corporate",
			  "foundedIn": "03/15/1994",
			  "businessSince": "08/02/2004",
			  "linkedCustomers": [
			    {
			      "mappindId": 0,
			      "parentCompanyId": 0,
			      "childCompanyId": 0,
			      "relationship": "Corporate Parent",
			      "subsidaryPercentage": 0,
			      name: 'Master GMBH', 
			      no: 11, 
			      location: 'Prag city', 
			      branch : 'Automotive',  
			      industry : '-'
			    },
			    {
			      "mappindId": 0,
			      "parentCompanyId": 0,
			      "childCompanyId": 0,
			      "relationship": "Corporate Parent",
			      "subsidaryPercentage": 0,
			      name: 'Master GMBH', 
			      no: 12, 
			      location: 'Prag city', 
			      branch : 'Automotive',  
			      industry : '-'
			    }
			  ]
			},
      {
			  "id": 12,
			  "name": "Muster GmbH 12",
			  "taxNumber": 4711,
			  "addressLine1": "Prag Street 12",
			  "addressLine2": "10999",
			  "location": "Berlin",
			  "legalForm": "GmBH",
			  "public": false,
			  "industryCodeISIC": "49410",
			  "industryCodeLocal": "49410",
			  "customerType": "Corporate",
			  "foundedIn": "03/15/1994",
			  "businessSince": "08/02/2004",
			  "linkedCustomers": [
			    {
			      "mappindId": 0,
			      "parentCompanyId": 0,
			      "childCompanyId": 0,
			      "relationship": "Corporate Parent",
			      "subsidaryPercentage": 0,
			      name: 'Master1 GMBH', 
			      no: 11, 
			      location: 'Prag city', 
			      branch : 'Automotive',  
			      industry : '-'
			    },
			    {
			      "mappindId": 0,
			      "parentCompanyId": 0,
			      "childCompanyId": 0,
			      "relationship": "Corporate Parent",
			      "subsidaryPercentage": 0,
			      name: 'Master1 GMBH', 
			      no: 12, 
			      location: 'Prag city', 
			      branch : 'Automotive',  
			      industry : '-'
			    }
			  ]
			},
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' },
      { id: 16, name: 'RubberMan' },
      { id: 17, name: 'Dynama' },
      { id: 18, name: 'Dr IQ' },
      { id: 19, name: 'Magma' },
      { id: 20, name: 'Tornado' }
    ];
    return {customers};
  }

  // Overrides the genId method to ensure that a customers always has an id.
  // If the customers array is empty,
  // the method below returns the initial number (11).
  // if the customers array is not empty, the method below returns the highest
  // customer id + 1.
  genId(customers: Customer[]): number {
    return customers.length > 0 ? Math.max(...customers.map(customer => customer.id)) + 1 : 11;
  }
}
