import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
  private gridApi;
  private gridColumnApi;
	columnDefs = [
      {headerName: 'Rating model', field: 'model', sortable: true, filter: 'agTextColumnFilter' },
      {headerName: 'Last Change', field: 'change', sortable: true, filter: true },
      {headerName: 'Aurthor', field: 'aurthor', sortable: true, filter: 'agDateColumnFilter'},
      {headerName: 'Created By', field: 'createdBy', sortable: true, filter: false},
      {headerName: 'Creation Date', field: 'createDate', sortable: true, filter: false},
      {headerName: 'Result', field: 'result', sortable: true, filter: true},
      {headerName: 'PD', field: 'pd', sortable: true, filter: true},
      {headerName: 'Rating Status', field: 'status', sortable: true, filter: true}
  ];

  rowData = [
      { model: 'Corporate with FA', change: '05/23/2018', aurthor: 'Martin', createdBy : 'Martin', createDate : '05/09/2018', result : 'AAA', pd : 0.01, status : '1' },
      { model: 'Corporate with FA', change: '05/23/2018', aurthor: 'Martin', createdBy : 'Martin', createDate : '05/09/2018', result : 'AAA', pd : 0.01, status : '2' },
      { model: 'Corporate with FA', change: '05/23/2018', aurthor: 'Martin', createdBy : 'Martin', createDate : '05/09/2018', result : 'AAA', pd : 0.01, status : '3' },
      { model: 'Corporate with FA', change: '05/23/2018', aurthor: 'Martin', createdBy : 'Martin', createDate : '05/09/2018', result : 'AAA', pd : 0.01, status : '4' },
      { model: 'Corporate with FA', change: '05/23/2018', aurthor: 'Martin', createdBy : 'Martin', createDate : '05/09/2018', result : 'AAA', pd : 0.01, status : '5' }
  ];

  constructor() { }

  ngOnInit() {
  }
	onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridColumnApi = params.columnApi;

  }  

}
