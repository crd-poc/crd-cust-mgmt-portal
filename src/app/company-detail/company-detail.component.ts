import { Component, OnInit, Input } from '@angular/core';
import { AvatarModule } from 'ng2-avatar';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent implements OnInit {
	customer: Customer;
	private _customerId: number;
	@Input() isDetail: boolean;

	@Input() 
	set customerId(id: number) {
		this._customerId = id;
		if(id)
			this.getCustomer(this._customerId);
	}

	get customerId() {
		return this._customerId;
	}	

  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit() {
  }

  getCustomer(id): void {
    this.customerService.getCustomer(id)
      .subscribe(customer => this.customer = customer);
  }

}
