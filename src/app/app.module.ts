import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AvatarModule } from 'ng2-avatar';
import { AgGridModule } from 'ag-grid-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupsComponent } from './groups/groups.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CompanyDetailComponent } from './company-detail/company-detail.component';
import { FinancialAnalysisComponent } from './financial-analysis/financial-analysis.component';
import { RatingComponent } from './rating/rating.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    GroupsComponent,
    PageNotFoundComponent,
    CompanyDetailComponent,
    FinancialAnalysisComponent,
    RatingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AvatarModule.forRoot(),
    AgGridModule.withComponents([]),
    NgbModule,
    HttpClientModule,
    //HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService)
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
