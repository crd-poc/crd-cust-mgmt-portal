import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Customer } from './customer';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
	private customersUrl = '/v1.0/customer';  // URL to web api
	
	/** Log a CustomerService message with the MessageService */
	private log(message: string) {
	  this.messageService.add(`CustomerService: ${message}`);
	}
  
  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET customer by id. Will 404 if id not found */
  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url).pipe(
      tap(_ => this.log(`fetched customer id=${id}`)),
      catchError(this.handleError<Customer>(`getCustomer id=${id}`))
    );
  }

  /** GET customers from the server */
	getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl)
      .pipe(
        tap(_ => this.log('fetched Customers')),
        catchError(this.handleError<Customer[]>('getCustomers', []))
      );
	} 

  /** GET linked customers from the server */
	getLinkedCustomers(id: number): Observable<Customer[]> {
		const url = `${this.customersUrl}/${id}/allLinkedCustomers`;
    return this.http.get<Customer[]>(url)
      .pipe(
        tap(_ => this.log('fetched Linked customer id=${id}')),
        catchError(this.handleError<Customer[]>('getLinkedCustomers', []))
      );
	}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }	 
}
