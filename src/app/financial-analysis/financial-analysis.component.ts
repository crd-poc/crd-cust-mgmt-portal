import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-financial-analysis',
  templateUrl: './financial-analysis.component.html',
  styleUrls: ['./financial-analysis.component.scss']
})
export class FinancialAnalysisComponent implements OnInit {
  private gridApi;
  private gridColumnApi;

	columnDefs = [
      {headerName: 'Level', field: 'level', sortable: true, filter: 'agSetColumnFilter' },
      {headerName: 'FA', field: 'fa', sortable: true, filter: true },
      {headerName: 'Reporting Date', field: 'date', sortable: true, filter: false},
      {headerName: 'Period', field: 'period', sortable: true, filter: true},
      {headerName: 'Fiscal', field: 'fiscal', sortable: true, filter: true},
      {headerName: 'FS Status', field: 'status', sortable: true, filter: true}
  ];

  rowData = [
      { level: 'Individual', fa: 'Annual Report', date: '09/10/2018', period : '12 months', fiscal : 'Annual-Final', status : 'Draft' },
      { level: 'Individual 1', fa: 'Annual Report', date: '09/10/2018', period : '12 months', fiscal : 'Annual-Final', status : 'Approved' },
      { level: 'Individual 2', fa: 'Annual Report', date: '09/10/2018', period : '12 months', fiscal : 'Annual-Final', status : 'Submitted' },
      { level: 'Individual 3', fa: 'Annual Report', date: '09/10/2018', period : '12 months', fiscal : 'Annual-Final', status : 'Submitted' }
  ];

  constructor() {
  }

  ngOnInit() {
  }
	onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridColumnApi = params.columnApi;

  }
}
